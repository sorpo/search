# -*- coding: utf-8 -*-

from configparser import LegacyInterpolation
from operator import invert
import random
import spacy
import re
import numpy as np
import pandas as pd
from nltk.corpus import stopwords

stopwords = set(stopwords.words('english'))

nlp = spacy.load("en_core_web_sm")

def lemmatize(query):
    doc = nlp(query)
    tokens = []
    for token in doc:
        tokens.append(token)

    return " ".join([token.lemma_ for token in doc])


def normalize(query):
    query = lemmatize(query)
    query = re.sub(r"[^ 'a-zA-Z]", "", query)
    query = query.lower().split()
    query = [word for word in query if word not in stopwords]
    return " ".join(query) 


class Document:
    def __init__(self, title, text):
        # можете здесь какие-нибудь свои поля подобавлять
        self.title = title
        self.text = text
    
    def format(self, query):
        # возвращает пару тайтл-текст, отформатированную под запрос
        return [self.title, self.text + ' ...']

index = []
inverted_index = []
df = pd.read_csv("data.csv")

def build_inverted_index():
    global inverted_index

    words = {'kek': [(0, 0)]}

    for index, text in df.Lyrics.items():
        unique, counts = np.unique(text.split(), return_counts=True)
        for word, count in zip(unique, counts):
            if word in words.keys():
                words[word] += [(index, count)]
            else:
                words.update({word: [(index, count)]})

    for word in words:
        words[word].sort(key=lambda x: x[1], reverse=True)
        words[word] = words[word][:1999]
    
    inverted_index = words


def build_index():
    global index
    kek = lambda x: Document(x[0], x[1])
    index = list(pd.Series(zip(list(df.Song), list(df.Lyrics))).apply(kek))
    print("!!!")
    
    # print(index[0].text)
    # считывает сырые данные и строит индекс
    # index.append(Document('The Beatle — Come Together', 'Yesturday come old flat top\nHe come groovin\' up slowly'))
    # index.append(Document('The Beatle — Yesturday', 'Here come old flat top\nHe come groovin\' up slowly'))
    # index.append(Document('The Rolling Stones — Brown Sugar', 'Yesturday Coast slave ship bound for cotton fields\nSold in the market down in New Orleans'))
    # index.append(Document('МС Хованский — Батя в здании', 'Вхожу в игру аккуратно,\nОна еще не готова.'))
    # index.append(Document('Физтех — Я променял девичий смех', 'Я променял девичий смех\nНа голос лектора занудный,'))

def score(query, document):
    query_ = nlp(query)
    similarity = query_.similarity(nlp(document.text))
    full_occurance = 0

    if query in normalize(document.title):
        full_occurance = 1
    
    # возвращает какой-то скор для пары запрос-документ
    # больше -- релевантнее
    return similarity + 0.5*full_occurance


def retrieve(query):
    # возвращает начальный список релевантных документов
    # (желательно, не бесконечный)
    # candidates = []
    # for doc in index:
    #     if query.lower() in doc.title.lower() or query in doc.text.lower():
    #         candidates.append(doc)
    # return candidates[:50]

    good = []
    for word in query.split():
        if word in inverted_index:
            indices, _ = zip(*inverted_index[word])
            if good==[]:
                good = indices
                good = set(good)
            else:
                good = good.intersection(indices)
    
    candidates = []
    # print("             index")
    # print(index)
    for i in good:
        candidates += [index[i]]

    for doc in index:
        if query in normalize(doc.title):
            candidates += [doc]

    return candidates[:100]